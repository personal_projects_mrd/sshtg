import os
import subprocess

from lib.microgram import MicroBot
from lib.abstract_models import Message


def main(bot: MicroBot):
    for update in bot.check_updates():
        if update is not None:
            args = update.message_text.split(" ")
            try:
                output = subprocess.run(args, capture_output=True)
            except Exception as e:
                bot.send_message(Message(chat_id=update.chat_id, text="python exception: " + str(e)))
                continue
            bot.send_message(Message(chat_id=update.chat_id, text="stdout: \n" + output.stdout.decode()))
            if output.stderr:
                bot.send_message(Message(chat_id=update.chat_id, text="stderr: \n" + output.stderr.decode()))


if __name__ == "__main__":
    tg_bot = MicroBot(token=os.getenv("token"))

    while 1:
        main(bot=tg_bot)
